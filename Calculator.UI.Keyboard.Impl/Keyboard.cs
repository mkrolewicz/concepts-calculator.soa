﻿using Calculator.UI.Keyboard.Contract;
using System;
using System.Windows.Controls;
using TinyMVVMHelper;

namespace Calculator.UI.Keyboard
{
    public class Keyboard : IKeyboard
    {
        public event EventHandler<KeyboardEventArgs> KeyPressed;
        public event EventHandler<EnableEventArgs> EnableChanged;

        private KeyboardControl control;
        private KeyboardViewModel model;

        public Keyboard()
        {
            control = new KeyboardControl();
            model = new KeyboardViewModel();
            model.ClickCommand.ExecuteAction = FireKeyPressed;
            model.PowerCommand.AfterExecution += FireEnableChanged;
            control.DataContext = model;
        }

        private void FireEnableChanged(object sender, CommandExecutionArgs e)
        {
            if (EnableChanged != null)
                EnableChanged(this, new EnableEventArgs(model.IsEnabled));
        }

        private void FireKeyPressed(object obj)
        {
            string key = obj as string;
            if (key != null && key.Length > 0)
            {
                if (KeyPressed != null)
                    KeyPressed(this, new KeyboardEventArgs(key));
            }
        }

        Control IKeyboard.Control
        {
            get { return control; }
        }


        bool IKeyboard.IsEnabled
        {
            get
            {
                return model.IsEnabled;
            }
        }
    }
}
