﻿using Calculator.ALU.Contract;
using Calculator.Core.Contract;
using Calculator.Memory.Contract;
using System.Collections.Generic;

namespace Calculator.Core
{
    public class Device : ICalculations, IMemoryOperations
    {
        private ICalculations calculations;
        private IMemoryOperations memoryOperations;

        public Device(IALU alu, IMemoryManager memoryManager, IRegisterOperator registerOperator)
        {
            var accumulator = memoryManager.GetOrCreateRegister("Accumulator");
            var lastOperation = memoryManager.GetOrCreateRegister("LastOperation");
            var memory = memoryManager.GetOrCreateRegister("InternalMemory");
            calculations = new Calculations(alu, registerOperator, accumulator, lastOperation);
            memoryOperations = new MemoryOperations(registerOperator, memory, accumulator);
        }

        public ICalculations GetCalculations() { return calculations; }

        public IMemoryOperations GetMemoryOperations() { return memoryOperations; }

        IEnumerable<Operation> ICalculations.AllowedOperations
        {
            get { return calculations.AllowedOperations; }
        }

        void ICalculations.PerformOperation(Operation operation, double number)
        {
            calculations.PerformOperation(operation, number);
        }

        double ICalculations.CurrentValue
        {
            get { return calculations.CurrentValue; }
        }

        void IMemoryOperations.MemoryClear()
        {
            memoryOperations.MemoryClear();
        }

        double IMemoryOperations.MemoryRetrive()
        {
            return memoryOperations.MemoryRetrive();
        }

        void IMemoryOperations.MemoryAdd(double number)
        {
            memoryOperations.MemoryAdd(number);
        }

        void IMemoryOperations.MemorySub(double number)
        {
            memoryOperations.MemorySub(number);
        }

        void IMemoryOperations.Reset()
        {
            memoryOperations.Reset();
        }
    }
}
