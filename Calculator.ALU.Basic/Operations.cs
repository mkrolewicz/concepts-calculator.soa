﻿using Calculator.ALU.Contract;

namespace Calculator.ALU.Basic
{
    class Add : ICalculateableOperation
    {
        public Operation Definiton { get; private set; }

        public Add()
        {
            Definiton = new Operation("Add", "+");
        }

        public double Calculate(double number1, double number2)
        {
            return number1 + number2;
        }
    }

    class Sub : ICalculateableOperation
    {
        public Operation Definiton { get; private set; }

        public Sub()
        {
            Definiton = new Operation("Add", "-");
        }
        
        public double Calculate(double number1, double number2)
        {
            return number1 - number2;
        }
    }

    class Mul : ICalculateableOperation
    {
        public Operation Definiton { get; private set; }
        
        public Mul()
        {
            Definiton = new Operation("Add", "×");
        }

        public double Calculate(double number1, double number2)
        {
            return number1 * number2;
        }
    }

    class Div : ICalculateableOperation
    {
        public Operation Definiton { get; private set; }

        public Div()
        {
            Definiton = new Operation("Add", "÷");
        }

        public double Calculate(double number1, double number2)
        {
            return number1 / number2;
        }
    }
}
