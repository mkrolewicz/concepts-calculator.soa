﻿using Calculator.Memory.Contract;

namespace Calculator.Memory
{
    class RegisterEntry
    {
        public Register RegisterId { get; private set; }

        object value;
        
        public RegisterEntry(string name)
        {
            RegisterId = new Register(name);
        }

        public object Retrive()
        {
            return value;
        }

        public void Store(object value)
        {
            this.value = value;
        }

        public void Clear()
        {
            value = null;
        }
    }
}
