﻿using Calculator.Memory.Contract;
using System;
using System.Collections.Generic;

namespace Calculator.Memory
{
    public class MemoryManager : IMemoryManager, IRegisterOperator
    {
        private IDictionary<string, RegisterEntry> registers;

        public MemoryManager()
        {
            registers = new Dictionary<string, RegisterEntry>();
        }

        Register IMemoryManager.CreateRegister(string name)
        {
            if (registers.ContainsKey(name))
                throw new ArgumentException(string.Format("Register with name {0} already existing", name));
            RegisterEntry register = new RegisterEntry(name);
            registers[name] = register;
            return register.RegisterId;
        }

        Register IMemoryManager.GetRegister(string name)
        {
            return registers[name].RegisterId;
        }

        Register IMemoryManager.GetOrCreateRegister(string name)
        {
            IMemoryManager mm = this;
            try
            {
                return mm.GetRegister(name);
            }
            catch (Exception ex)
            {
                return mm.CreateRegister(name);
            }
        }

        object IRegisterOperator.Retrive(Register register)
        {
            return registers[register.Name].Retrive();
        }

        void IRegisterOperator.Store(Register register, object value)
        {
            registers[register.Name].Store(value);
        }

        void IRegisterOperator.Clear(Register register)
        {
            registers[register.Name].Clear();
        }
    }
}
