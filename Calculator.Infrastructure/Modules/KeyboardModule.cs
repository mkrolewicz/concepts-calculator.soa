﻿using Calculator.UI.Keyboard.Contract;
using Ninject.Modules;

namespace Calculator.Infrastructure.Modules
{
    class KeyboardModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IKeyboard>().To<Calculator.UI.Keyboard.Keyboard>().InSingletonScope();
        }
    }
}
